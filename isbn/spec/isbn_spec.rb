# ISBN-10 is made up of 9 digits plus a check digit (which may be "X").
# Spaces and hyphens may be included in a code, but are not significant.
# This means that 9780471486480 is equivalent to 978-0-471-48648-0 and 978 0 471 48648 0.

# The check digit for ISBN-10 is calculated by multiplying each digit by
# its position (i.e., 1 x 1st digit, 2 x 2nd digit, etc.), summing these products together
# and taking modulo 11 of the result (with "X" being used if the result is 10).

# Valid examples: 0471958697, 0 471 95869 7, 0-471-95869-7, 155404295X
# Invalid examples:
# * 0471958697 (wrong verifier)
# * 0471958 (wrong length)
# * a471958697 (wrong digit)

# Write logic to valid? if a given string is a valid ISBN-10.

require 'rspec'
require_relative '../model/isbn'

describe 'Isbn' do
  let(:isbn) { Isbn.new }

  it 'verificar isbn-10 de vacio deberia ser falso' do
    expect(isbn.valid?('')).to eq false
  end

  it 'verificar isbn-10 valido sin espacios deberia ser verdadero' do
    expect(isbn.valid?('9992158107')).to eq true
  end

  it 'verificar isbn-10 valido con espacios deberia ser verdadero' do
    expect(isbn.valid?('9971 5 0210 0')).to eq true
  end

  it 'verificar isbn-10 valido con guion deberia ser verdadero' do
    expect(isbn.valid?('960-425-059-0')).to eq true
  end

  it 'verificar isbn-10 valido con guion deberia ser verdadero' do
    expect(isbn.valid?('0-684-84328-5')).to eq true
  end

  it 'verificar isbn-10 valido (sin espacios) con "X" deberia ser verdadero' do
    expect(isbn.valid?('999210810X')).to eq true
  end

  it 'verificar isbn-10 valido (con espacios) con "X" deberia ser verdadero' do
    expect(isbn.valid?('1304 1 3489 X')).to eq true
  end

  it 'verificar isbn-10 valido (con guion) con "X" deberia ser verdadero' do
    expect(isbn.valid?('999-210-810-x')).to eq true
  end

  it 'verificar isbn-10 con dos guiones seguidos deberia ser falso' do
    expect(isbn.valid?('960-425--059-0')).to eq false
  end

  it 'verificar isbn-10 con dos espacios seguidos deberia ser falso' do
    expect(isbn.valid?('9971 5 0210  0')).to eq false
  end

  it 'verificar isbn-10 (sin espacios) de <10 digitos deberia ser falso' do
    expect(isbn.valid?('11569')).to eq false
  end

  it 'verificar isbn-10 (con espacios) de <10 digitos deberia ser falso' do
    expect(isbn.valid?('11 569 2')).to eq false
  end

  it 'verificar isbn-10 (con guion) de <10 digitos deberia ser falso' do
    expect(isbn.valid?('11-569')).to eq false
  end

  it 'verificar isbn-10 (sin espacios) de >10 digitos deberia ser falso' do
    expect(isbn.valid?('99921581007')).to eq false
  end

  it 'verificar isbn-10 (con espacios) de >10 digitos deberia ser falso' do
    expect(isbn.valid?('9971 5 02100 0')).to eq false
  end

  it 'verificar isbn-10 (con guion) de >10 digitos deberia ser falso' do
    expect(isbn.valid?('0-684-843280-5')).to eq false
  end

  it 'verificar isbn-10 con caracteres deberia ser falso' do
    expect(isbn.valid?('960-4b5-059-5')).to eq false
  end

  it 'verificar isbn-10 con caracter distinto de "X" deberia ser falso' do
    expect(isbn.valid?('999-210-810-c')).to eq false
  end

  it 'verificar isbn-10 con separador "_" deberia ser invalido' do
    expect(isbn.valid?('960_425_059_0')).to eq false
  end

  it 'verificar isbn-10 con caracter "X" y modulo distinto de 10 deberia ser falso' do
    expect(isbn.valid?('999-210-800-x')).to eq false
  end
end
