class Isbn

  ISBN_LENGTH = 10
  DELIMITER_SPACE = ' '
  DELIMITER_HYPHEN = '-'

  def valid?(code)
    if code.to_s.empty?
      return false
    end

    if wrong_chars?(code)
      return false
    end

    if code.include?(DELIMITER_SPACE)
      return validate_code_by(code, DELIMITER_SPACE)
    end

    if code.include?(DELIMITER_HYPHEN)
      return validate_code_by(code, DELIMITER_HYPHEN)
    end

    return validate_code_by(code, '')
  end

private

  def validate_code_by(code,delimiter)
    total_sum = 0
    char_index = 1
    last_digit = 0

    trim_code = code.tr(delimiter, '').upcase

    if trim_code.length != ISBN_LENGTH
      return false
    end

    trim_code.each_char do |char|

      if char_index < ISBN_LENGTH
        total_sum += char.to_i * char_index
        char_index += 1
      else
        last_digit = number?(char) ? char.to_i : 10
      end
    end

    verification_digit = total_sum % 11

    if verification_digit != last_digit
      return false
    end

    return true
    
  end

  def number?(number)
    number =~ /[0-9]/
  end

  def wrong_chars?(code)
    code =~ /[^-0-9Xx\ ]|[-\ ]{2,}/
  end
end
