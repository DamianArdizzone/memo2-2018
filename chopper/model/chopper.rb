class Chopper
  
  NUMBER_ES = ["cero","uno","dos","tres","cuatro","cinco","seis","siete","ocho","nueve"]
  EMPTY_STR = "vacio"
  ERROR_TOO_BIG = "demasiado grande"

  def chop(n, array)
    
    if array.empty?
    	return -1
	end

	return array.index(n)

  end

  def sum(array)
    
    if array.empty?
    	return EMPTY_STR
	end

	sum = 0
	
	array.each do |element|
		sum+= element
  	end
  	
  	return self.number_to_string_es(sum)
  end

  def number_to_string_es(number)
    
    if number >= 100
    	return ERROR_TOO_BIG
  	end
    
    if number < 10
    	return NUMBER_ES[number]
  	end

	first = number.div(10)
	second = number % 10

	return NUMBER_ES[first]+","+NUMBER_ES[second]
  
  end

end