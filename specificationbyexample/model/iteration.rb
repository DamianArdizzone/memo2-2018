class Iteration
  attr_reader :name
  attr_reader :calification
  attr_writer :calification

  def initialize(name)
    @name = name
    @calification = :unrated
  end
end
