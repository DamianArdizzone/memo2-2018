require_relative 'project'
require_relative 'task'

MIN_ATTENDANCE = 0.75
MIN_ITERATIONS = 3

class Student
  attr_reader :name
  attr_reader :status
  attr_reader :assigned_tasks
  attr_reader :assigned_project
  attr_reader :attendance
  attr_writer :status
  attr_writer :attendance

  def initialize(name)
    @name = name
    @status = :irregular
    @assigned_tasks = []
    @assigned_project = nil
    @attendance = 0
  end

  def assign_task(task)
    @assigned_tasks.push(task)
  end

  def assign_project(project)
    @assigned_project = project
  end

  def pass?
    good_attendance? && tasks_approved? && project_approved?
  end

  private

  def good_attendance?
    @attendance >= MIN_ATTENDANCE
  end

  def tasks_approved?
    total = 0
    @assigned_tasks.each do |task|
      if task.approved?
        total += 1
      end
    end
    total == assigned_tasks.length
  end

  def project_approved?
    @assigned_project.approved_iterations.length >= MIN_ITERATIONS
  end
end
