class Task
  attr_reader :status
  attr_writer :status

  def initialize
    @status = :pending
  end

  def assign_to(student)
    student.assign_task(self)
  end

  def approved?
    @status == :approved
  end
end
