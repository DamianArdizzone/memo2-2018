require_relative 'iteration'
class Project
  attr_reader :iterations
  attr_reader :name

  def initialize(name)
    @iterations = []
    @name = name
  end

  def assign_to(student)
    student.assign_project(self)
  end

  def create_iteration(name)
    @iteration = Iteration.new(name)
    @iterations.push(@iteration)
    @iteration
  end

  def approved_iterations
    @approved_iterations = []
    @iterations.each do |iteration|
      iteration.calification == :approved ? @approved_iterations.push(iteration) : ''
    end
    @approved_iterations
  end
end
