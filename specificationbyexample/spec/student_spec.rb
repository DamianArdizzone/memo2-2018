require 'rspec'
require_relative '../model/student'

describe 'Student' do
  let(:student) { Student.new('Carlos') }

  it 'create student with a name' do
    expect(student.name).to eq 'Carlos'
  end

  it 'initial student status is irregular' do
    expect(student.status).to eq :irregular
  end

  it 'change student status' do
    student.status = :regular
    expect(student.status).to eq :regular
  end

  it 'created student should not have tasks' do
    expect(student.assigned_tasks).to be_empty
  end

  it 'change student attendance' do
    student.attendance = 0.5
    expect(student.attendance).to eq 0.5
  end

  it 'created student should not have attendance' do
    expect(student.attendance).to eq 0
  end

end
