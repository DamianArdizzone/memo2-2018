require 'rspec'
require_relative '../model/project'
require_relative '../model/student'

describe 'Project' do
  before do
    @project = Project.new('Projecto 1')
    @student = Student.new('Jose')
  end

  it 'create a proect with a name' do
    expect(@project.name).to eq 'Projecto 1'
  end

  it 'a new project should not have iterations' do
    expect(@project.iterations).to be_empty
  end

  it 'assign a Project to a Student, the Student should has the Project' do
    @project.assign_to(@student)
    expect(@student.assigned_project.name).to eq @project.name
  end

  it 'create an Iteration, the Project should has the Iteration' do
    @iteration = @project.create_iteration('Test')
    expect(@project.iterations).to include(@iteration)
  end

  it 'create two Iterations, the Project should has the Iterations' do
    @iteration1 = @project.create_iteration('Test 1')
    @iteration2 = @project.create_iteration('Test 2')
    expect(@project.iterations).to (include(@iteration1) && include(@iteration2))
  end

  it 'create two Iterations, the Project should has the Iterations' do
    @iteration3 = @project.create_iteration('Test 3')
    @iteration4 = @project.create_iteration('Test 4')
    @iteration3.calification = :approved
    expect(@project.approved_iterations).not_to include(@iteration4)
  end

end
