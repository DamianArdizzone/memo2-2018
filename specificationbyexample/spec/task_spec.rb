require 'rspec'
require_relative '../model/task'
require_relative '../model/student'

describe 'Task' do
  before do
    @task = Task.new
    @student = Student.new('Pepe')
  end

  it 'a new task should have status pending' do
    expect(@task.status).to eq :pending
  end

  it 'change a task status should be approved' do
    @task.status = :approved
    expect(@task.status).to eq :approved
  end

  it 'assign a Task to a Student, the Student should has the Task' do
    @task.assign_to(@student)
    expect(@student.assigned_tasks).to include(@task)
  end

  it 'If the Task is approved, task_approved? should be true' do
    @task.status = :approved
    expect(@task.approved?).to eq true
  end
end
