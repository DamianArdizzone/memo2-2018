require 'rspec'
require_relative '../model/iteration'

describe 'Iteration' do
  let(:iteration) { Iteration.new('Sprint 1') }

  it 'create iteration with a name' do
    expect(iteration.name).to eq 'Sprint 1'
  end

  it 'initial iteration calification is unrated' do
    expect(iteration.calification).to eq :unrated
  end

  it 'change iteration calification' do
    iteration.calification = :approved
    expect(iteration.calification).to eq :approved
  end
end
