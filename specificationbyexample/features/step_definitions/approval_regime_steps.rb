Given(/^el alumno Juan$/) do
  @student = Student.new('Juan')
end

Given(/^la tarea individual ta$/) do
  @task_a = Task.new
  @task_a.assign_to(@student)
end

Given(/^la tarea individual tb$/) do
  @task_b = Task.new
  @task_b.assign_to(@student)
end

Given(/^la tarea individual tc$/) do
  @task_c = Task.new
  @task_c.assign_to(@student)
end

Given(/^el proyecto grupal pg$/) do
  @project = Project.new('Proyecto Test')
  @project.assign_to(@student)

  @iteration1 = @project.create_iteration('Iteration-1')
  @iteration2 = @project.create_iteration('Iteration-2')
  @iteration3 = @project.create_iteration('Iteration-3')
  @iteration4 = @project.create_iteration('Iteration-4')
end

Given(/^el alumno regular Juan$/) do
  @student.status = :regular
end

When(/^aprobo todas las tareas semanales$/) do
  @task_a.status = :approved
  @task_b.status = :approved
  @task_c.status = :approved
end

When(/^no aprobo la tarea individual tc$/) do
  @task_c.status = 'disapproved'
end

When(/^asistio al (\d+) % de las clases$/) do |arg1|
  @student.attendance = arg1.to_f/100
end

When(/^aprobo([A-z\ ]*) (\d+) iteraciones del proyecto pg$/) do |arg1,arg2|

  @iterations = @project.iterations
  total = arg2.to_i - 1
  for i in 0..total
    @iterations[i].calification = :approved
  end
end

Then(/^aprobo la materia$/) do
  expect(@student.pass?).to eq true
end

Then(/^No aprobo la materia$/) do
  expect(@student.pass?).to eq false
end
