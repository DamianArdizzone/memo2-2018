Feature: Regimen de aprobacion

  Background:
    Given el alumno Juan
    Given la tarea individual ta
    Given la tarea individual tb
    Given la tarea individual tc
    Given el proyecto grupal pg

  Scenario: alumno aprobado
    Given el alumno regular Juan
    When aprobo todas las tareas semanales
    And asistio al 75 % de las clases
    And aprobo al menos 3 iteraciones del proyecto pg
    Then aprobo la materia

  Scenario: alumno desaprobado por inasistencias
    Given el alumno regular Juan
    When aprobo todas las tareas semanales
    And asistio al 60 % de las clases
    And aprobo al menos 3 iteraciones del proyecto pg
    Then No aprobo la materia
 
  Scenario: alumno desaprobado por tareas individuales
    Given el alumno regular Juan
    When no aprobo la tarea individual tc
    Then No aprobo la materia
 
  Scenario: alumno desaprobado por proyecto grupal
    Given el alumno regular Juan
    When aprobo todas las tareas semanales
    And asistio al 75 % de las clases
    And aprobo 2 iteraciones del proyecto pg
    Then No aprobo la materia
