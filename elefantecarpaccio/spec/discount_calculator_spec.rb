require 'rspec'
require_relative '../model/discount_calculator'

describe 'discount Calculator' do
  let(:discount_calculator) { DiscountCalculator.new }

  it '3 products costs 10 should return 30' do
    expect(discount_calculator.compute(3, 10)).to eq 30
  end

  it '3 products costs 10.45 should return 31.35' do
    expect(discount_calculator.compute(3, 10.45)).to eq 31.35
  end

  it 'total >1000 should return result with discount of 3%' do
    expect(discount_calculator.compute(20, 55)).to eq 1067
  end

  it 'total==1000 should return result with discount of 3%' do
    expect(discount_calculator.compute(50, 20)).to eq 970
  end

  it 'total >5000 should return result with discount of 5%' do
    expect(discount_calculator.compute(15, 340)).to eq 4845
  end

  it 'total >7000 should return result with discount of 7%' do
    expect(discount_calculator.compute(300.2, 25)).to eq 6979.65
  end

  it 'total >10_000 should return result with discount of 10%' do
    expect(discount_calculator.compute(150, 68)).to eq 9180
  end

  it 'total >50_000 should return result with discount of 15%' do
    expect(discount_calculator.compute(50, 1100)).to eq 46_750
  end

  it 'total from UT should return result with taxes for UT(6.85%)' do
    expect(discount_calculator.compute(50, 10, 'UT')).to eq 534.25
  end

  it 'total from NV should return result with taxes for NV(8%)' do
    expect(discount_calculator.compute(2, 50, 'NV')).to eq 108
  end

  it 'total from TX should return result with taxes for TX(6.25%)' do
    expect(discount_calculator.compute(1, 100, 'TX')).to eq 106.25
  end

  it 'total from AL should return result with taxes for AL(4%)' do
    expect(discount_calculator.compute(50, 2, 'AL')).to eq 104
  end

  it 'total from CA should return result with taxes for CA(8.25%)' do
    expect(discount_calculator.compute(1, 10, 'CA')).to eq 10.83
  end

  it 'total>1000 from AL should return result with taxes for AL(4%) and discount of 3%' do
    expect(discount_calculator.compute(50, 30, 'AL')).to eq 1513.2
  end
end
