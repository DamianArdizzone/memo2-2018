require File.expand_path 'spec_helper.rb', __dir__

describe 'Elefante Carpaccio' do
  it 'get / should return an url message' do
    get '/'
    expect(last_response).to be_ok
    expect(last_response.body).to include 'Use /total'
  end

  it '/total of 3 prod and price should return products * price (GET)' do
    get '/total?products=3&price=10'
    parsed_body = JSON.parse(last_response.body)
    expect(last_response).to be_ok
    expect(parsed_body['total']).to eq 30
  end

  it '/total total >50_000  should return result with discount (GET)' do
    get '/total?products=50&price=1100'
    parsed_body = JSON.parse(last_response.body)
    expect(last_response).to be_ok
    expect(parsed_body['total']).to eq 46_750
  end

  it '/total from UT should return result with taxes for UT (GET)' do
    get '/total?products=5&price=100&state=UT'
    parsed_body = JSON.parse(last_response.body)
    expect(last_response).to be_ok
    expect(parsed_body['total']).to eq 534.25
  end

  it '/total total=10000 from TX should return result with taxes for TX and discount of 10% (GET)' do
    get '/total?products=10&price=1000&state=TX'
    parsed_body = JSON.parse(last_response.body)
    expect(last_response).to be_ok
    expect(parsed_body['total']).to eq 9562.5
  end
end
# curl localhost:4567/elefantecarpaccio?
