class DiscountCalculator
  DISCOUNTS = { 1000 => 0.97, 5000 => 0.95, 7000 => 0.93, 10_000 => 0.90, 50_000 => 0.85 }.freeze
  TAXES = { 'UT' => 1.0685, 'NV' => 1.08, 'TX' => 1.0625, 'AL' => 1.04, 'CA' => 1.0825 }.freeze
  DECIMALS = 2

  def compute(products, price, *state)
    total = products * price
    total_with_discount = total
    DISCOUNTS.each do |base, discount|
      total_with_discount = total >= base ? (discount * total) : total_with_discount
    end
    total_with_taxes = !state[0].nil? ? TAXES[state[0]] * total_with_discount : total_with_discount
    total_with_taxes.round(DECIMALS)
  end
end
