require 'sinatra'
require 'json'
require_relative '../elefantecarpaccio/model/discount_calculator'

get '/' do
  'Use /total'
end

get '/total' do
  discountCalculator = DiscountCalculator.new
  products = params['products'].to_f
  price = params['price'].to_f
  state = params['state']
  content_type :json
  { total: discountCalculator.compute(products, price, state) }.to_json
end
