# spec/app_spec.rb

require File.expand_path '../spec_helper.rb', __FILE__

describe 'Sinatra Application' do

  it 'get / should return an url message' do
    get '/'
    expect(last_response).to be_ok
    expect(last_response.body).to include 'Use /chop or /sum'
  end

  it '/chop of 3 and [0,7,3] should return the same parameters (POST)' do
    post '/chop', { 'x':'3', 'y':'0,7,3'}
    parsed_body = JSON.parse(last_response.body)
    expect(last_response).to be_ok
    expect(parsed_body['chop']).to eq 'x=3,y=0,7,3'
  end

  it '/chop of 3 and [0,7,3] should return result eq 2 (POST)' do
    post '/chop', { 'x':'3', 'y':'0,7,3'}
    parsed_body = JSON.parse(last_response.body)
    expect(last_response).to be_ok
    expect(parsed_body['result']).to eq 2
  end

  it '/sum [9,9] should return the same parameters (GET)' do
    get '/sum?x=9,9'
    parsed_body = JSON.parse(last_response.body)
    expect(last_response).to be_ok
    expect(parsed_body['sum']).to eq '9,9'
  end

  it '/sum [9,9] should return result uno,ocho (GET)' do
    get '/sum?x=9,9'
    parsed_body = JSON.parse(last_response.body)
    expect(last_response).to be_ok
    expect(parsed_body['result']).to eq 'uno,ocho'
  end
end

# curl localhost:4567/hola?nombre=nico
# curl -X POST localhost:4567/chau -d 'nombre=nico'