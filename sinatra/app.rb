require 'sinatra'
require 'json'
require_relative '../sinatra/model/chopper'

get '/' do
  'Use /chop or /sum'
end

post '/chop' do
  chopper = Chopper.new
  x = params['x']
  y = params['y']
  array = y.split(',').map { |s| s.to_i }
  content_type :json
  {:chop=> "x=#{x},y=#{y}", :result => chopper.chop(x.to_i,array) }.to_json
end

get '/sum' do
  chopper = Chopper.new
  x = params['x']
  array = x.split(',').map { |s| s.to_i }
  content_type :json
  {:sum => "#{x}", :result => chopper.sum(array) }.to_json
end
